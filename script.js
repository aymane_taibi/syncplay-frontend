'use strict'
const URL = window.URL || window.webkitURL
const fileSelectorLabel = document.querySelector('.file-choose-container>label');
const selectedFileDetailsUl = document.querySelector('.selected-file-details');
const fileInput = document.querySelector('#video-file-browser');
const mainVideoPlayer = document.querySelector('#main-video-player');
const sipSessionConfigurationContainer = document.querySelector('.sip-session-configuration-container');
const sipUsername = document.querySelector('#sipUsername');
const sipPeername = document.querySelector('#sipPeername');
const callConfirmContainer = document.querySelector('.call-confirm-container');
const callPeerContainer = document.querySelector('.call-peer-container');
const playBackProgressBarContainer = document.querySelector('.play-back-progress-bar-container');
const videoProgressSeekPreview = document.querySelector('.video-progress-seek-preview');

const loadSelectedFile = () => {
    if(fileInput.files.length === 0) return;
    showFileDetails(fileInput.files[0]);
}
const showFileDetails = file => {
    fileSelectorLabel.style.display = "none";
    selectedFileDetailsUl.style.display = "block";
    selectedFileDetailsUl.innerHTML = `
        <li>
            <i class="fas fa-tag"></i> ` + file.name + `
        </li>
        <li>
            <i class="far fa-hdd"></i> ` + converttoHumanReadableSize(file.size) + `
        </li>
        <li>
            <a class="confirm-selected-button" href="javascript:backToFileSelector()">
                <i class="fas fa-times-circle fa-2x"></i>
            </a>
            <a class="confirm-selected-button" href="javascript:confirmPlayingFile()">
                <i class="fas fa-check-circle fa-2x"></i>
            </a>
        </li>
    `;
}
const converttoHumanReadableSize = fileSize => {
    const units = [" b", " Kb", " Mb", " Gb"];
    let unitIndex = 0;
    while(fileSize > 1024 && unitIndex < 3){
        fileSize /= 1024;
        unitIndex++;
    }
    return fileSize.toFixed(2) + units[unitIndex];
}
const converttoHumanReadableTime = unix_timestamp => {
    var date = new Date(unix_timestamp * 1000);
    var hours = date.getHours() - 1;
    var minutes = "0" + date.getMinutes();
    var seconds = "0" + date.getSeconds();
    var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
    return formattedTime;
}
const backToFileSelector = () => {
    fileSelectorLabel.style.display = "block";
    selectedFileDetailsUl.style.display = "none";
    sipSessionConfigurationContainer.style.display = "none";
    fileInput.value = "";
}
const confirmPlayingFile = () => {
    sipPeername.value = localStorage.getItem('sipPeername');
    sipUsername.value = localStorage.getItem('sipUsername');
    sipSessionConfigurationContainer.style.display = "block";
    selectedFileDetailsUl.style.display = "none";
}
const setSipUsername = () => {
    if(!/^[a-zA-Z_]*$/.test(sipUsername.value)){
        alert("Invalid username.\nMust container only a-z, A-Z and _");
        return;
    }
    if(!/^[a-zA-Z_]*$/.test(sipPeername.value)){
        alert("Invalid peername.\nMust container only a-z, A-Z and _");
        return;
    }
    localStorage.setItem('sipPeername', sipPeername.value);
    localStorage.setItem('sipUsername', sipUsername.value);
    initializeSipSession(sipUsername.value);
    mainVideoPlayer.src = URL.createObjectURL(fileInput.files[0]);
    mainVideoPlayer.style.display = "block";
    sipSessionConfigurationContainer.style.display = "none";
}

document.body.onkeypress = e => {
    switch(e.keyCode){
        case 32:
            videoControls.playPauseVideo();
            break;
        case 39:
            videoControls.forwardBy5secs();
            break;
        case 37:
            videoControls.rewindBy5secs();
            break;
        case 38:
            videoControls.volumeIncrease();
            break;
        case 40:
            videoControls.volumeDecrease();
            break;
        case 102:
            videoControls.goFullScreen();
            break;
    }
}

const videoControls = {
    forwardBy5secs: () => {
        mainVideoPlayer.currentTime += 5;
    },
    rewindBy5secs: () => {
        mainVideoPlayer.currentTime -= 5;
    },
    volumeDecrease: () => {
        mainVideoPlayer.volume -= 0.05;
    },
    volumeIncrease: () => {
        mainVideoPlayer.volume += 0.05;
    },
    playVideo: () => {
        if(mainVideoPlayer.paused){
            mainVideoPlayer.play();
        }
    },
    seekTo: time => { 
        mainVideoPlayer.currentTime = time
    },
    pauseVideo: () => {
        if(!mainVideoPlayer.paused){
            mainVideoPlayer.pause();
        }
    },
    playPauseVideo: () => {
        mainVideoPlayer.paused
        ? mainVideoPlayer.play()
        : mainVideoPlayer.pause();
    },
    volumeMuteUnmute: () => {
        mainVideoPlayer.muted = !mainVideoPlayer.muted;
    },
    goFullScreen: () => {
        mainVideoPlayer.requestFullscreen();
    }
}

const controls = {
    playPauseButton: document.querySelector('.playPauseButton'),
    volumeDecreaseButton: document.querySelector('.volumeDecreaseButton'),
    volumeIncreaseButton: document.querySelector('.volumeIncreaseButton'),
    playBackProgressBar: document.querySelector('.play-back-progress-bar'),
    videoProgress: document.querySelector('.video-progress'),
    videoDuration: document.querySelector('.video-duration'),
    currentVolumeValue: document.querySelector('.current-volume-value'),
}

playBackProgressBarContainer.addEventListener('mouseout', e => {
});
playBackProgressBarContainer.addEventListener('mousemove', e => {
    videoProgressSeekPreview.innerHTML = converttoHumanReadableTime(mainVideoPlayer.duration*e.clientX/e.target.clientWidth);
});

playBackProgressBarContainer.addEventListener('click', e => {
    mainVideoPlayer.currentTime = mainVideoPlayer.duration*e.clientX/e.target.clientWidth;
    sipUser.message("sip:" + sipPeername.value + "@sipjs.onsip.com", JSON.stringify({
        type: "action",
        value: "seek",
        time: mainVideoPlayer.currentTime,
    }))
});

mainVideoPlayer.addEventListener('volumechange', e => {
    controls.currentVolumeValue.innerHTML = Math.round(mainVideoPlayer.volume*100) + " %"
});
mainVideoPlayer.addEventListener('play', e => {
    controls.playPauseButton.innerHTML = '<i class="fas fa-pause-circle fa-2x"></i>';
    sipUser.message("sip:" + sipPeername.value + "@sipjs.onsip.com", JSON.stringify({
        type: "action",
        value: "play"
    }))
});
mainVideoPlayer.addEventListener('pause', e => {
    controls.playPauseButton.innerHTML = '<i class="fas fa-play-circle fa-2x"></i>';
    sipUser.message("sip:" + sipPeername.value + "@sipjs.onsip.com", JSON.stringify({
        type: "action",
        value: "pause"
    }))
});
mainVideoPlayer.addEventListener('timeupdate', e => {
    controls.playBackProgressBar.style.width = (mainVideoPlayer.currentTime/mainVideoPlayer.duration*100)+"%";
    controls.videoProgress.innerHTML = converttoHumanReadableTime(mainVideoPlayer.currentTime);
});
mainVideoPlayer.addEventListener('durationchange', e => {
    controls.videoDuration.innerHTML = converttoHumanReadableTime(mainVideoPlayer.duration);
});
mainVideoPlayer.addEventListener('seeked', e => {
});

var sipUser = undefined;
var hasCall = false;
const initializeSipSession = (username) => {
    sipUser = new SIP.Web.SimpleUser('wss://edge.sip.onsip.com', {

        media: {
            constraints: {
                audio: true,
                //video: true
            },
            remote: {
                audio: new Audio()
            }
        },
        aor: "sip:"+username+"@sipjs.onsip.com",
        userAgentOptions: {
            displayName: username,
            userAgentString: SIP.name + "." + SIP.version + " sipjs.com"
        },
        delegate: {
            onMessageReceived: processMessage,
            onCallReceived: () => {
                callConfirmContainer.style.top = "0px";
                callPeerContainer.style.top = "-60px";
            },
            onCallHangup: () => {
                callConfirmContainer.style.top = "-60px";
                callPeerContainer.style.top = "0px";
                hasCall = false;
            },
            onCallAnswered: () => {
                callConfirmContainer.style.top = "-60px";
                hasCall = true;
            }
        }
    });
    sipUser.connect().then(() => {
        sipUser.register().then(() => {
            callPeerContainer.style.top = "0px";
        });
    });
}
const processMessage = (message) => {
    let payload = JSON.parse(message);
    
    switch(payload.type){
        case "action":
            switch(payload.value){
                case "play":
                    videoControls.playVideo();
                    break;
                case "pause":
                    videoControls.pauseVideo();
                    break;
                case "seek":
                    videoControls.seekTo(payload.time);
                    break;
            }
            break;
    }
}
const callControls = {
    answerCall: () => {
        sipUser.answer();
    },
    declineCall: () => {
        sipUser.decline()
    },
    callPeer: () => {
        sipUser.call("sip:" + sipPeername.value + "@sipjs.onsip.com");
    }
}